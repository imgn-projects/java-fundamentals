
public class ControlFlow {
	public static void main(String args[]) {

		int mathGrade = 90;
		
		System.out.println("Basic IF statement:");
		if (mathGrade >= 90) {
			System.out.println("A+");
		}

		System.out.println("Basic if else statement results: ");
		if (mathGrade == 80) {
			System.out.print("B");
		}
		else {
			System.out.println("A+");
		}

		System.out.println("If else if else statement results:");
		int testscore = 76;
		char grade;

		if (testscore >= 90) {
			grade = 'A';
		} else if (testscore >= 80) {
			grade = 'B';
		} else if (testscore >= 70) {
			grade = 'C';
		} else if (testscore >= 60) {
			grade = 'D';
		} else {
			grade = 'F';
		}
		System.out.println("Grade = " + grade);

	
		System.out.println("Switch statement results:");
		int month = 8;
		String monthString;
		switch (month) {
		case 1:
			monthString = "January";
			break;
		case 2:
			monthString = "February";
			break;
		case 3:
			monthString = "March";
			break;
		case 4:
			monthString = "April";
			break;
		case 5:
			monthString = "May";
			break;
		case 6:
			monthString = "June";
			break;
		case 7:
			monthString = "July";
			break;
		case 8:
			monthString = "August";
			break;
		case 9:
			monthString = "September";
			break;
		case 10:
			monthString = "October";
			break;
		case 11:
			monthString = "November";
			break;
		case 12:
			monthString = "December";
			break;
		default:
			monthString = "Invalid month";
			break;
		}
		
		System.out.println(monthString);
		
		boolean bool = false;
		boolean apple = true;
		
		if(bool && apple) {
			System.out.println("Bool and apple are true.");
		} else { 
			System.out.println("One of bool or apple are not true.");
		}
		
		if(bool | apple) {
			System.out.println("Bool or apple are true.");
		} else {
			System.out.println("Both bool and apple are false");
		}
		
		if(mathGrade != 80) {
			System.out.println("B");
		} else {
			System.out.println("Is equal to 80");
		}
	}
}
