
public class Looping {
	public static void main(String[] args) {
		System.out.println("For loop with a int variable");
		for (int number = 1; number < 11; number++) {
			System.out.println("Count is: " + number);
		}

		System.out.println("For each loop with a string variable");
		String[] names = new String[5];

		names[0] = "Terry";
		names[1] = "Henriko";
		names[2] = "Dane";
		names[3] = "Ruhan";
		names[4] = "Gordy";

		for (String name : names) {
			System.out.println("Name: " + name);
		}

		System.out.println("While loop with a int variable");
		int count = 1;
		while (count < 11) {
			System.out.println("Count is: " + count);
			count++;
		}

		System.out.println("Do loop with a int variable:");
		int doWhileCount = 1;
		do {
			System.out.println("Count is: " + doWhileCount);
			doWhileCount++;
		} while (doWhileCount < 11);
	}
}