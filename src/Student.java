
public class Student {
	private String name;
	private Integer age;
	
	public Student() {
		System.out.println("This is the default constructor.");
	}

	public Student(String name, Integer age) {
		this.name = name;
		this.age = age;
	}

	public void printBase() {
		System.out.println("This is your basic method.");
	}

	public void printBase(String text) {
		System.out.println(text);
	}
	
	public void printBase(Integer value) {
		System.out.println(value * 2);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	
}
