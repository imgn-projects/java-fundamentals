
public class Functions {
	private static final Double VAT = 0.14;
	
	public static void main(String[] args) {
		Price eggs = new Price();
		eggs.value = 10D;
		Price bread = new Price();
		bread.value = 12D;
		bread.vatApplicable = false;
		
		Price[] prices = new Price[2];
		prices[0] = eggs;
		prices[1] = bread;
		
		for (Price price : prices) {
			if (price.vatApplicable) {
				price.value = addVat(price.value);
			}
			System.out.println(price.value);
		}
	}
	
	private static Double addVat(Double price) {
		Double vat = price * VAT;
		return price + vat;
	}
}
