
public class ArraysExplained {
	public static void main (String [] args) {
		int[] watever = new int[4];
		
		watever[0] = 2;
		watever[1] = 8;
		watever[2] = 6;
		watever[3] = 4;
		
		System.out.println(watever[0]);
		System.out.println(watever[1]);
		System.out.println(watever[2]);
		System.out.println(watever[3]);
		
		System.out.println("count" + watever.length);
		
		String[] names = new String[5];
		
		names[0] = "Terry";
		names[1] = "Henriko";
		names[2] = "Dane";
		names[3] = "Ruhan";
		names[4] = "Gordy";
		
		System.out.println(names[0]);
		System.out.println(names[1]);
		System.out.println(names[2]);
		System.out.println(names[3]);
		System.out.println(names[4]);
		
		int[][] table = new int[3][4];
		
		table[0][0] = 20;
		table[1][0] = 23;
		table[2][0] = 50;
		
		table[0][1] = 17;
		table[0][2] = 36;
		table[0][3] = 48;
		
		System.out.println(table[0][0] + "\t" + table[0][1]);
		System.out.println(table[1][0] + "\t" + table[0][2]);
		System.out.println(table[2][0] + "\t" + table[0][3]);
	}
}
