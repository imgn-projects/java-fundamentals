
public class FunctionsRevised {

    public static void main(String args[]) {
        printBase();
        
        System.out.println(returnMethod());
    }
    
    public static void printBase() {
        System.out.println("This is your basic method.");
    }
    
    public static String returnMethod() {
        return "This is returned";
    }
}
