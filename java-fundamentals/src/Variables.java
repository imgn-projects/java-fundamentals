
public class Variables {
	static final Integer someInteger = 0;
	
	public static void main(String args[]) {
		int i = 0;
		double d = 1.6;
		float myFloatValue = 0;
		short myShort = 0;
		byte b = 127;
		long l = 0;
		boolean myBool = true;
		char c = 'c';
		String s = "s";
		
		i = (int)d;
		
		System.out.println(i + d);
		System.out.println(i);
		System.out.println(d);
		System.out.println(myFloatValue);
		System.out.println(myShort);
		System.out.println(b);
		System.out.println(l);
		System.out.println(myBool);
		
		Integer y = i;
		Double dO = d;
		Float fO = myFloatValue;
		Boolean bO = myBool;
		Short sO = myShort;
		Long lO = l;
		Byte byteO = b;
	}
	

}
